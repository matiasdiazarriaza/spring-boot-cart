package cl.diaz.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

@Component
public class ReportUtil {
	private static final Logger logger = LoggerFactory.getLogger(ReportUtil.class);

	@Autowired
	DataSource datasource;

	public byte[] getData(String reportName, Map<String, Object> parameters, String type) {
		Connection connection = null;
		try {
			connection = datasource.getConnection();

			final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			InputStream reportStream = getClass().getResourceAsStream("/reports/" + reportName + ".jrxml");

			JasperReport jasperReport = JasperCompileManager.compileReport(reportStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);

			switch (type.toUpperCase()) {
				case "XLS":
					JRXlsxExporter exporterXLS = new JRXlsxExporter();
					exporterXLS.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(outStream));
					SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
					reportConfig.setRemoveEmptySpaceBetweenRows(true);
					exporterXLS.setConfiguration(reportConfig);
					exporterXLS.exportReport();
					break;
				case "PDF":
					JRPdfExporter exporterPDF = new JRPdfExporter();
					exporterPDF.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporterPDF.setExporterOutput(new SimpleOutputStreamExporterOutput(outStream));
					exporterPDF.exportReport();
					break;
				case "CSV":
					JRCsvExporter exporterCSV = new JRCsvExporter();
					exporterCSV.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporterCSV.setExporterOutput(new SimpleWriterExporterOutput(outStream));
					exporterCSV.exportReport();
					break;
				case "HTML":
					HtmlExporter exporterHTML = new HtmlExporter();
					exporterHTML.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporterHTML.setExporterOutput(new SimpleHtmlExporterOutput(outStream));
					exporterHTML.exportReport();
					break;
				default:
					break;
			}
			return outStream.toByteArray();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return new byte[0];
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException ex) {
				logger.error(ex.getMessage());
			}
		}
	}
}