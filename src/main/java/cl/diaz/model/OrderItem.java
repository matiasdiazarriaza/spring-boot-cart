package cl.diaz.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "ORDER_ITEM")
public class OrderItem {
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated OrderItem ID")
    private long id;

    @ApiModelProperty(notes = "This is ID Order of OrderItem")
    private long idOrder; 

    @ApiModelProperty(notes = "This is the quantity of Product in the OrderItem")
    private int quantity;

    @ApiModelProperty(notes = "This is ID_color of OrderItem")
    private long idColor;

    @ApiModelProperty(notes = "This is ID_product of OrderItem")
    private long idProduct;


    public OrderItem(){
        this.id = 0;
        this.quantity = 0;
        this.idColor = 0;
        this.idOrder = 0;
        this.idProduct = 0;
    }

    public OrderItem(long id, int quantity, long idColor, long idOrder, long idProduct) {
        this.id = id;
        this.quantity = quantity;
        this.idColor = idColor;
        this.idOrder = idOrder;
        this.idProduct = idProduct;
    }
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
   
    public long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(long idOrder) {
        this.idOrder = idOrder;
    }

    public long getIdColor() {
        return idColor;
    }

    public void setIdColor(long idColor) {
        this.idColor = idColor;
    }

    public long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(long idProduct) {
        this.idProduct = idProduct;
    }

    



    
}