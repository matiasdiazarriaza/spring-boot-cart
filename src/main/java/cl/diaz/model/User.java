package cl.diaz.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;



@Entity
@Table(name = "USER")
public class User {
    

    
    @Id
    @ApiModelProperty(notes = "The database generated User ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @ApiModelProperty(notes = "The database generated User ID")
    private long idRol;

    @ApiModelProperty(notes = "This is name of the User")
    private String name;

    @ApiModelProperty(notes = "This is surname of the User")
    private String surname;

    @ApiModelProperty(notes = "This is born date of the User")
    private Date bornDate;

    @ApiModelProperty(notes = "This is email of the User")
    private String email;

    @ApiModelProperty(notes = "This is password of the User")
    private String password;

    @ApiModelProperty(notes = "This is the state of the User")
    private boolean state;

    @ApiModelProperty(notes = "This is a image of the User")
    private byte[] image;

    @ApiModelProperty(notes = "This is a balance of the User")
    private int balance;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "idUser", insertable = false, nullable = false, updatable = false)
    private List<Order> orders;




    public User(){
        this.id = 0;
        this.idRol = 0;
        this.name = null;
        this.surname = null;
        this.bornDate = null;
        this.email = null;
        this.password = null;
        this.state = false;
        this.image = null;
        this.balance = 0;
        this.orders = null;
       
    }

    public User(long id, long idRol, String name, String surname, Date bornDate, String email, String password, boolean state,
    byte[] image, int balance, List<Order> orders ) {
        this.id = id;
        this.idRol = idRol;
        this.name = name;
        this.surname = surname;
        this.bornDate = bornDate;
        this.email = email;
        this.password = password;
        this.state = state;
        this.image = image;
        this.balance = balance;
        this.orders = orders;
       
    }

    


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public long getIdRol() {
        return idRol;
    }

    public void setIdRol(long idRol) {
        this.idRol = idRol;
    }

    
   

}