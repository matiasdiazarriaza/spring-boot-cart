package cl.diaz.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "PRODUCT")
public class Product {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Product ID")
    private long id;


    @ApiModelProperty(notes = "This is name of the Product")
    private String nombre;

    @ApiModelProperty(notes = "This is sku of the Product")
    private int sku;

    @ApiModelProperty(notes = "This is description of the Product")
    private String description;

    @ApiModelProperty(notes = "This is price of the Product")
    private int price;

    @ApiModelProperty(notes = "This is state of the Product")
    private boolean state;

    @ApiModelProperty(notes = "This is populare of the Product")
    private boolean populare;

    @ApiModelProperty(notes = "This is the colors of the Product")
    @ManyToMany
    private List<Color> colors;

    @ApiModelProperty(notes = "This is id category of the Product")
    private long idCategory;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "idProduct", insertable = false, nullable = false, updatable = false)
    private List<OrderItem> orderItems;



    public Product(){
        this.id = 0;
        this.nombre = null;
        this.sku = 0;
        this.description = null;
        this.price = 0;
        this.state = false;
        this.populare = false;
        this.idCategory = 0;
        this.colors = null;
        this.orderItems = null;
    }

    public Product(long id, String nombre, int sku, String description, int price, boolean state, boolean populare, List<Color> colors,
            long idCategory, List<OrderItem> orderItems) {
        this.id = id;
        this.nombre = nombre;
        this.sku = sku;
        this.description = description;
        this.price = price;
        this.state = state;
        this.populare = populare;
        this.colors = colors;
        this.idCategory = idCategory;
        this.orderItems = orderItems;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSku() {
        return sku;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean isPopulare() {
        return populare;
    }

    public void setPopulare(boolean populare) {
        this.populare = populare;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    


    

    
}