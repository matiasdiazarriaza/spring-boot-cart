package cl.diaz.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name = "ORDEN")
public class Order {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated Order ID")
    private long id;
    
    @ApiModelProperty(notes = "This is start date of the Order")
    private Date startDate;

    @ApiModelProperty(notes = "This is buy date of the Order")
    private Date dateBuy;

    @ApiModelProperty(notes = "This is if the Order is ordered or not")
    private boolean ordered;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "idUser", insertable = false, nullable = false, updatable = false)
    private List<OrderItem> orderItems;

    @ApiModelProperty(notes = "This is ID_User of Order")
    private long idUser;
   


    public Order(){
        this.id = 0;
        this.startDate = null;
        this.dateBuy = null;
        this.ordered = false;
        this.idUser = 0;
        
    }


    public Order(long id, Date startDate, Date dateBuy, boolean ordered, long idUser) {
        this.id = id;
        this.startDate = startDate;
        this.dateBuy = dateBuy;
        this.ordered = ordered;
        this.idUser = idUser;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDateBuy() {
        return dateBuy;
    }

    public void setDateBuy(Date dateBuy) {
        this.dateBuy = dateBuy;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }



  

    


}