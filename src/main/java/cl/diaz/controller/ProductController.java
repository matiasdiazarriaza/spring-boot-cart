package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.diaz.model.Product;
import cl.diaz.services.ProductServices.ProductServices;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ProductController {
    
    @Autowired
    private ProductServices productServices;


    @GetMapping("/products")
    public List<Product> index(){
        return productServices.findAll();
    }

    @GetMapping("/products/page/{page}")
    public Page<Product> index(@PathVariable Integer page){
        return productServices.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Product product = null;

        Map<String, Object> response = new HashMap<>();

        try {
            product = productServices.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (product == null) {
            response.put("mensaje", "El producto ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Product>(product, HttpStatus.OK);
       
    }


    @PostMapping("/products")
    public ResponseEntity<?> create(@Valid @RequestBody Product product, BindingResult result){
        Product productnew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream().map(err -> {
                return "El campo '" + err.getField() + "' " + err.getDefaultMessage();
            }).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            productnew = productServices.save(product);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡El producto a sido creado con exito!");
        response.put("producto", productnew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Product product, BindingResult result, @PathVariable Long id) {
        Product productn = productServices.findById(id);
        Product productUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (productn == null) {
            response.put("mensaje", "Error: No se pudo actualizar, el producto ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            productn.setNombre(product.getNombre());
            productn.setDescription(product.getDescription());
            productn.setSku(product.getSku());
            productn.setIdCategory(product.getIdCategory());
            productn.setPrice(product.getPrice());
            productn.setState(product.isState());
            productn.setPopulare(product.isPopulare());
            productUpdate = productServices.save(productn);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El producto a sido actualizado correctamente");
        response.put("producto", productUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Product productDelete = productServices.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            productServices.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El producto a sido eliminado correctamente");
        response.put("producto", productDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }
}