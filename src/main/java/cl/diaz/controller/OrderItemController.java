package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.diaz.model.OrderItem;
import cl.diaz.services.OrderItemServices.OrderItemService;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class OrderItemController {
    
    @Autowired
    private OrderItemService orderItemService;

    @GetMapping("/orderItems")
    public List<OrderItem> index(){
        return orderItemService.findAll();
    }

    @GetMapping("/orderItems/page/{page}")
    public Page<OrderItem> index(@PathVariable Integer page){
        return orderItemService.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/orderItems/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        OrderItem orderItem = null;

        Map<String, Object> response = new HashMap<>();

        try {
            orderItem = orderItemService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (orderItem == null) {
            response.put("mensaje", "El orderItem ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<OrderItem>(orderItem, HttpStatus.OK);
       
    }


    @PostMapping("/orderItems")
    public ResponseEntity<?> create(@Valid @RequestBody OrderItem orderItem, BindingResult result){
        OrderItem orderItemnew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream().map(err -> {
                return "El campo '" + err.getField() + "' " + err.getDefaultMessage();
            }).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            orderItemnew = orderItemService.save(orderItem);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡El Order Item a sido creado con exito!");
        response.put("orderItem", orderItemnew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @PutMapping("/orderItems/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody OrderItem orderItem, BindingResult result, @PathVariable Long id) {
        OrderItem orderItemn = orderItemService.findById(id);
        OrderItem orderItemUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (orderItemn == null) {
            response.put("mensaje", "Error: No se pudo actualizar, el Order Item ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            orderItemn.setIdOrder(orderItem.getIdOrder());
            orderItemn.setQuantity(orderItem.getQuantity());
            orderItemn.setIdColor(orderItem.getIdColor());
            orderItemn.setIdProduct(orderItem.getIdProduct());

            
            orderItemUpdate = orderItemService.save(orderItemn);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El Order Item a sido actualizado correctamente");
        response.put("orderItem", orderItemUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }

    @DeleteMapping("/orderItems/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        OrderItem orderDelete = orderItemService.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            orderItemService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El Order Item a sido eliminado correctamente");
        response.put("orderItem", orderDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }
}