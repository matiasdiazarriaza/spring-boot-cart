package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.diaz.model.Order;
import cl.diaz.services.OrderServices.OrderService;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class OrderController {


    @Autowired
    private OrderService orderService;


    @GetMapping("/orders")
    public List<Order> index(){
        return orderService.findAll();
    }

    @GetMapping("/orders/page/{page}")
    public Page<Order> index(@PathVariable Integer page){
        return orderService.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Order order = null;

        Map<String, Object> response = new HashMap<>();

        try {
            order = orderService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (order == null) {
            response.put("mensaje", "La orden ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Order>(order, HttpStatus.OK);
       
    }


    @PostMapping("/orders")
    public ResponseEntity<?> create(@Valid @RequestBody Order order, BindingResult result){
        Order ordernew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream().map(err -> {
                return "El campo '" + err.getField() + "' " + err.getDefaultMessage();
            }).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            ordernew = orderService.save(order);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡La orden a sido creada con exito!");
        response.put("orden", ordernew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Order order, BindingResult result, @PathVariable Long id) {
        Order ordern = orderService.findById(id);
        Order orderUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (ordern == null) {
            response.put("mensaje", "Error: No se pudo actualizar la orden ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            ordern.setStartDate(order.getStartDate());
            ordern.setDateBuy(order.getDateBuy());
            ordern.setOrdered(order.isOrdered());
            ordern.setIdUser(order.getIdUser());
       
            orderUpdate = orderService.save(ordern);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "La orden a sido actualizada correctamente");
        response.put("orden", orderUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Order orderDelete = orderService.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            orderService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "La orden a sido eliminada correctamente");
        response.put("order", orderDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }
    
}