package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.diaz.model.Category;
import cl.diaz.services.CategoryServices.CategoryService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class CategoryController {
    
    @Autowired
    private CategoryService categoryService;


    @GetMapping("/categories")
    public List<Category> index(){
        return categoryService.findAll();
    }

    @GetMapping("/categories/page/{page}")
    public Page<Category> index(@PathVariable Integer page){
        return categoryService.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Category category = null;

        Map<String, Object> response = new HashMap<>();

        try {
            category = categoryService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (category == null) {
            response.put("mensaje", "La categoria ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Category>(category, HttpStatus.OK);
       
    }


    @PostMapping("/categories")
    public ResponseEntity<?> create(@Valid @RequestBody Category category, BindingResult result){
        Category categorianew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream().map(err -> {
                return "El campo '" + err.getField() + "' " + err.getDefaultMessage();
            }).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            categorianew = categoryService.save(category);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡La categoria a sido creado con exito!");
        response.put("categoria", categorianew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Category category, BindingResult result, @PathVariable Long id) {
        Category categorian = categoryService.findById(id);
        Category categoriaUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (categorian == null) {
            response.put("mensaje", "Error: No se pudo actualizar la categoria ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            categorian.setName(category.getName());
          
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "La categoria a sido actualizado correctamente");
        response.put("categoria", categoriaUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Category categoriaDelete = categoryService.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            categoryService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "La categoria a sido eliminado correctamente");
        response.put("categoria", categoriaDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }
}