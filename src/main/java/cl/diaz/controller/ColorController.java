package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.diaz.model.Color;
import cl.diaz.services.ColorServices.ColorService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ColorController {
    

    @Autowired
    private ColorService colorService;


    @GetMapping("/colors")
    public List<Color> index(){
        return colorService.findAll();
    }

    @GetMapping("/colors/page/{page}")
    public Page<Color> index(@PathVariable Integer page){
        return colorService.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/colors/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Color color = null;

        Map<String, Object> response = new HashMap<>();

        try {
            color = colorService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (color == null) {
            response.put("mensaje", "El color ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Color>(color, HttpStatus.OK);
       
    }


    @PostMapping("/colors")
    public ResponseEntity<?> create(@Valid @RequestBody Color color, BindingResult result){
        Color colornew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream().map(err -> {
                return "El campo '" + err.getField() + "' " + err.getDefaultMessage();
            }).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            colornew = colorService.save(color);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡El color a sido creado con exito!");
        response.put("color", colornew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @PutMapping("/colors/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Color color, BindingResult result, @PathVariable Long id) {
        Color colorn = colorService.findById(id);
        Color colorUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (colorn == null) {
            response.put("mensaje", "Error: No se pudo actualizar, el color ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            colorn.setName(color.getName());
            colorn.setColors(color.getColors());
          
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El color a sido actualizado correctamente");
        response.put("color", colorUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }

    @DeleteMapping("/colors/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Color colorDelete = colorService.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            colorService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El color a sido eliminado correctamente");
        response.put("color", colorDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }

}