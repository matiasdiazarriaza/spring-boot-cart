package cl.diaz.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import cl.diaz.model.Role;
import cl.diaz.services.RoleServices.RoleService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class RoleController {
    

    @Autowired
    private RoleService roleService;


    @GetMapping("/roles")
    public List<Role> index(){
        return roleService.findAll();
    }

    @GetMapping("/roles/page/{page}")
    public Page<Role> index(@PathVariable Integer page){
        return roleService.findAll(PageRequest.of(page,2));
    }

    @GetMapping("/roles/{id}")
    public ResponseEntity<?> show(@PathVariable Long id){
        Role role = null;

        Map<String, Object> response = new HashMap<>();

        try {
            role = roleService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al hacer la consulta en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (role == null) {
            response.put("mensaje", "El rol ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Role>(role, HttpStatus.OK);
    }

    @PostMapping("/roles")
    public ResponseEntity<?> create(@Valid @RequestBody Role role, BindingResult result){
        Role rolenew = null;
        Map<String, Object> response = new HashMap<>();

        try {
            rolenew = roleService.save(role);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar el insert en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "¡El rol a sido creado con exito!");
        response.put("rol", rolenew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

        
    }

    @PutMapping("/roles/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Role role, BindingResult result, @PathVariable Long id) {
        Role roln = roleService.findById(id);
        Role rolUpdate = null;
        Map<String, Object> response = new HashMap<>();

        if (roln == null) {
            response.put("mensaje", "Error: No se pudo actualizar el rol ID: "
                    .concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            roln.setName(role.getName());
            rolUpdate = roleService.save(roln);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El rol a sido actualizado correctamente");
        response.put("rol", rolUpdate);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);


    }
    @DeleteMapping("/roles/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Role roleDelete = roleService.findById(id);
        Map<String, Object> response = new HashMap<>();

        try {
            roleService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en la base de datos");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El rol a sido eliminado correctamente");
        response.put("rol", roleDelete);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

    }
}