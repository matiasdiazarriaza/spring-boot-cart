package cl.diaz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.diaz.model.Order;


@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    
}