package cl.diaz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.diaz.model.Color;



@Repository
public interface ColorRepository extends JpaRepository<Color, Long> {

}