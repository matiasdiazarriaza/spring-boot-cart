package cl.diaz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.diaz.model.Role;



@Repository
public interface RoleRepository extends JpaRepository <Role,Long> {
    
}