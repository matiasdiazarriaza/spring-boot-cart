package cl.diaz.services.UserServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.diaz.model.User;


public interface UserService {
    
    public List<User> findAll();

    public Page<User> findAll(Pageable pageable);

    public User findById(Long id);

    public User save(User user);

    public void delete(Long id);

}