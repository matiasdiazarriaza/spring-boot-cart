package cl.diaz.services.OrderItemServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.diaz.model.OrderItem;

public interface OrderItemService {

    public List<OrderItem> findAll();

    public Page<OrderItem> findAll(Pageable pageable);

    public OrderItem findById(Long id);

    public OrderItem save(OrderItem orderItem);

    public void delete(Long id);
}