package cl.diaz.services.OrderItemServices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cl.diaz.model.OrderItem;
import cl.diaz.repository.OrderItemRepository;


@Service
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    private OrderItemRepository repository;

    @Override
    public List<OrderItem> findAll() {
       return (List<OrderItem>)repository.findAll();
    }

    @Override
    public Page<OrderItem> findAll(Pageable pageable) {
      return repository.findAll(pageable);
    }

    @Override
    public OrderItem findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public OrderItem save(OrderItem orderItem) {
        return repository.save(orderItem);
    }

    @Override
    public void delete(Long id) {
     repository.deleteById(id);
    }

}
    
