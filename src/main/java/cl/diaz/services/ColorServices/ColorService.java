package cl.diaz.services.ColorServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.diaz.model.Color;

public interface ColorService {

    public List<Color> findAll();

    public Page<Color> findAll(Pageable pageable);

    public Color findById(Long id);

    public Color save(Color category);

    public void delete(Long id);
}