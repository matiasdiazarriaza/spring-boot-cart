package cl.diaz.services.ColorServices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cl.diaz.model.Color;
import cl.diaz.repository.ColorRepository;

@Service
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorRepository repository;

    @Override
    public List<Color> findAll() {
       return (List<Color>)repository.findAll();
    }

    @Override
    public Page<Color> findAll(Pageable pageable) {
        return (Page<Color>)repository.findAll(pageable);
    }

    @Override
    public Color findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Color save(Color category) {
        return repository.save(category);
    }

    @Override
    public void delete(Long id) {
        
     repository.deleteById(id);

    }
    
}