package cl.diaz.services.CategoryServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.diaz.model.Category;

public interface CategoryService  {
    
    public List<Category> findAll();

    public Page<Category> findAll(Pageable pageable);

    public Category findById(Long id);

    public Category save(Category category);

    public void delete(Long id);


}