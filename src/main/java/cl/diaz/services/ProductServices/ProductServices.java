package cl.diaz.services.ProductServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import cl.diaz.model.Product;



public interface ProductServices {
    
    public List<Product> findAll();

    public Page<Product> findAll(Pageable pageable);

    public Product findById(Long id);

    public Product save(Product product);

    public void delete(Long id);

}