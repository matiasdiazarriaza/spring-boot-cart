package cl.diaz.services.RoleServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import cl.diaz.model.Role;



public interface RoleService {
    
    public List<Role> findAll();

    public Page<Role> findAll(Pageable pageable);

    public Role findById(Long id);

    public Role save(Role role);

    public void delete(Long id);

}