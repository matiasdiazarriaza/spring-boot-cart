package cl.diaz.services.OrderServices;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.diaz.model.Order;



public interface OrderService {
    
    public List<Order> findAll();

    public Page<Order> findAll(Pageable pageable);

    public Order findById(Long id);

    public Order save(Order order);

    public void delete(Long id);
}